#include "syscall.h"

int main()
{   
   int sum1=0,sum2=0;
   int x,i,y,z;
   int sleep_start, sleep_end;
   int N=40;
   x = system_call_Fork();
   if (x == 0) {
      system_call_PrintInt(sum1);
      y = system_call_Fork();
      if(y==0){
         for(i=1;i<=10;i++){
            system_call_PrintInt(i);
            sum1+=i;
         }
         system_call_PrintInt(sum1);
         system_call_Exit(sum1);
      }
      else{
         system_call_PrintInt(sum1);
         for(i=11;i<=20;i++){
            sum1+=i;
         }
         system_call_PrintInt(sum1);
         sum1 += system_call_Join(y);
         system_call_Exit(sum1);
      }
   }
//   **********************************************************    
   else {
         int m = system_call_Join(x);
      system_call_PrintInt(sum2);
      z = system_call_Fork();
      if(z==0){
        system_call_PrintInt(sum2);
        for(i=21;i<=30;i++){
            system_call_PrintInt(i);
            sum2+=i;
         }
         system_call_Exit(sum2);

      }
      else{
         system_call_PrintInt(sum2);
         for(i=31;i<=40;i++){
            sum2+=i;
         }
         sum2 += system_call_Join(z);
         system_call_PrintChar('M');
         system_call_PrintInt(m);
         system_call_PrintChar('\n');
         system_call_PrintInt(sum2);
         system_call_PrintChar('\n');
         system_call_PrintChar('D');
         system_call_PrintInt(sum2+m);
         system_call_PrintChar('\n');
      }
   }
   return 0;
}

