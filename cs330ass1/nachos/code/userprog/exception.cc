// exception.cc 
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.  
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "stdio.h"
#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "console.h"
#include "synch.h"

//----------------------------------------------------------------------
// ExceptionHandler
// 	Entry point into the Nachos kernel.  Called when a user program
//	is executing, and either does a syscall, or generates an addressing
//	or arithmetic exception.
//
// 	For system calls, the following is the calling convention:
//
// 	system call code -- r2
//		arg1 -- r4
//		arg2 -- r5
//		arg3 -- r6
//		arg4 -- r7
//
//	The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//	"which" is the kind of exception.  The list of possible exceptions 
//	are in machine.h.
//----------------------------------------------------------------------

void startFunc(int arg);

static Semaphore *readAvail;
static Semaphore *writeDone;
static void ReadAvail(int arg) { readAvail->V(); }
static void WriteDone(int arg) { writeDone->V(); }

static void ConvertIntToHex (unsigned v, Console *console)
{
   unsigned x;
   if (v == 0) return;
   ConvertIntToHex (v/16, console);
   x = v % 16;
   if (x < 10) {
      writeDone->P() ;
      console->PutChar('0'+x);
   }
   else {
      writeDone->P() ;
      console->PutChar('a'+x-10);
   }
}

void
ExceptionHandler(ExceptionType which)
{
    int type = machine->ReadRegister(2);
    int memval, vaddr, printval, tempval, exp;
    unsigned printvalus;        // Used for printing in hex
    if (!initializedConsoleSemaphores) {
       readAvail = new Semaphore("read avail", 0);
       writeDone = new Semaphore("write done", 1);
       initializedConsoleSemaphores = true;
    }
    Console *console = new Console(NULL, NULL, ReadAvail, WriteDone, 0);;

    if ((which == SyscallException) && (type == SYScall_Halt)) {
	DEBUG('a', "Shutdown, initiated by user program.\n");
   	interrupt->Halt();
    }
    else if ((which == SyscallException) && (type == SYScall_PrintInt)) {
       printval = machine->ReadRegister(4);
       if (printval == 0) {
	  writeDone->P() ;
          console->PutChar('0');
       }
       else {
          if (printval < 0) {
	     writeDone->P() ;
             console->PutChar('-');
             printval = -printval;
          }
          tempval = printval;
          exp=1;
          while (tempval != 0) {
             tempval = tempval/10;
             exp = exp*10;
          }
          exp = exp/10;
          while (exp > 0) {
	     writeDone->P() ;
             console->PutChar('0'+(printval/exp));
             printval = printval % exp;
             exp = exp/10;
          }
       }
       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if ((which == SyscallException) && (type == SYScall_PrintChar)) {
	writeDone->P() ;
        console->PutChar(machine->ReadRegister(4));   // echo it!
       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if ((which == SyscallException) && (type == SYScall_PrintString)) {
       vaddr = machine->ReadRegister(4);
       machine->ReadMem(vaddr, 1, &memval);
       while ((*(char*)&memval) != '\0') {
	  writeDone->P() ;
          console->PutChar(*(char*)&memval);
          vaddr++;
          machine->ReadMem(vaddr, 1, &memval);
       }
       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if ((which == SyscallException) && (type == SYScall_PrintIntHex)) {
       printvalus = (unsigned)machine->ReadRegister(4);
       writeDone->P() ;
       console->PutChar('0');
       writeDone->P() ;
       console->PutChar('x');
       if (printvalus == 0) {
          writeDone->P() ;
          console->PutChar('0');
       }
       else {
          ConvertIntToHex (printvalus, console);
       }
       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if((which == SyscallException) && (type == SYScall_GetReg))
    {
       // Determine which register to read the value from
       int reg = machine->ReadRegister(4);
       // Read the value in corresponding register and write to register 2
       machine->WriteRegister(2,machine->ReadRegister(reg));

       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if((which == SyscallException) && (type == SYScall_GetPA))
    {
       int i;
       unsigned int vpn, offset;
       TranslationEntry *entry;
       unsigned int pageFrame;
       int viradd = machine->ReadRegister(4);
       int* phyaddp = new int;
       
       // we must have either a TLB or a page table, but not both!
       ASSERT(machine->tlb == NULL || machine->NachOSpageTable == NULL);	
       ASSERT(machine->tlb != NULL || machine->NachOSpageTable != NULL);	

   // calculate the virtual page number, and offset within the page,
   // from the virtual address
       vpn = (unsigned) viradd / PageSize;
       offset = (unsigned) viradd % PageSize;
      if(machine->tlb == NULL){
         if (vpn >= machine->pageTableSize || !machine->NachOSpageTable[vpn].valid) {
             machine->WriteRegister(2,-1);
         }
         entry = &machine->NachOSpageTable[vpn];
      }
      else{
         for(entry = NULL, i=0;i< TLBSize; i++)
            if(machine->tlb[i].valid && (machine->tlb[i].virtualPage == vpn)){
               entry = &machine->tlb[i];
               break;
            }
      }
      if (entry == NULL) {				// not found
            machine->WriteRegister(2,-1);
      }

       pageFrame = entry->physicalPage;

       // if the pageFrame is too big, there is something really wrong! 
       // An invalid translation was loaded into the page table or TLB. 
       if (pageFrame >= NumPhysPages) { 
            machine->WriteRegister(2,-1);
       }
       
       *phyaddp = pageFrame * PageSize + offset;
       machine->WriteRegister(2, *phyaddp);
       
       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if((which == SyscallException) && (type == SYScall_GetPID))
       {
       // Get the PID of the current running thread
       int pid = currentThread->getPID();
       // Write the PID to register 2
       machine->WriteRegister(2,pid);
       
       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if((which == SyscallException) && (type == SYScall_GetPPID))
    {
       // Get the PPID of the current running thread
       int ppid = currentThread->getPPID();
       // Write the PID to register 2
       machine->WriteRegister(2,ppid);

       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if((which == SyscallException) && (type == SYScall_Time)){
       // Get the total ticks passed till now from the stats
       int totTime = stats->totalTicks;
       // Write the totTime to register 2
       machine->WriteRegister(2, totTime);
       
       // Advance program counters.
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if((which == SyscallException) && (type == SYScall_Sleep)){
       
       // Get the total time to sleep for
       int waitTime = machine->ReadRegister(4);
       
       if(waitTime==0){
          currentThread->YieldCPU();
       }
       else{
          // Set the interrupts off before putting the thread to sleep
          IntStatus oldLevel = interrupt->SetLevel(IntOff);
          
          // Get the total time passed till now and the current thread, which has to be put to sleep
          int curTime = stats->totalTicks;
          NachOSThread* oldThread = currentThread;

          
          // Move the current thread pointer to sleep queue with being the wakeup time for the thread and put the thread to sleep
          scheduler -> MoveToSleepQueue(currentThread, waitTime+curTime);
          oldThread -> PutThreadToSleep();
          
          // Set the interrupt level back to the original level before sleeping
          interrupt->SetLevel(oldLevel);
       }
       //Advance program counters
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if ((which == SyscallException) && (type == SYScall_Yield)){
       // Get the currently running thread
       NachOSThread* cur = currentThread;
       
       // Calling the yield thread function
       cur -> YieldCPU();

       //Advance program counters
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if ((which == SyscallException) && (type == SYScall_NumInstr)){
       // Get the total number of instruction executed and write to register 2
       machine->WriteRegister(2, currentThread->getInstNo());

       //Advance program counters
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else if ((which == SyscallException) && (type == SYScall_Exit)){
       // Obtain the value of exitcode
       int Ecode = machine->ReadRegister(4);
       // Get the current pid and ppid
       int pid = currentThread->getPID();
       int parpid = machine->pidData[1][pid];

       // Change the ppid of all the children of the current thread who are not already dead
       for(int i=1;i<TotalPID;i++){
         if(machine->pidData[1][i] == pid && machine->pidData[0][i] != -2){
            machine->pidData[1][i]=0;
            currentThread->pidPointer[i]->setPPID(0);
         }
       }
       // Set the status of current thread to dead and set its exit code
       machine->pidData[0][pid] = -2;
       machine->pidData[2][pid] = Ecode;
       
       // If the parent of current thread is sleeping and it is also waiting for the current thread then wake it up
       if(machine->pidData[0][parpid] == -1 && currentThread->pidPointer[parpid]->waitForPID == pid){
         IntStatus oldLevel = interrupt->SetLevel(IntOff);
         scheduler->ThreadIsReadyToRun(currentThread->pidPointer[parpid]);
         (void)interrupt->SetLevel(oldLevel);
       }
       // If there exist other threads still running call finish Thead else halt the machine
       if(machine->livePid>1){
         currentThread -> FinishThread();
       }
       
       interrupt->Halt();
    }
    else if ((which == SyscallException) && (type == SYScall_Exec)){
       int vaddr =machine->ReadRegister(4);
       int start = vaddr;
      
       char * file = new char[100];
       machine->ReadMem(vaddr, 1, &memval);
       
      //reading file name 
       while ((*(char*)&memval) != '\0') {
          file[vaddr-start]=*(char*)&memval;
          vaddr++;
          machine->ReadMem(vaddr, 1, &memval);
       }
       
       // Create executable
       OpenFile * executable = fileSystem->Open(file);

       ProcessAddrSpace * space;
       if(executable == NULL){
         printf("Unable to open file %s\n",file);
       }
      // Delete the current ProcessAddrSpace
       delete currentThread->space;

       // Create a new ProcessAddrSpace and assign it to the current thread
       space = new ProcessAddrSpace(executable);
       currentThread->space = space;
       // Initialize the cpu registers and assign the new Page table to machine
       space->InitUserCPURegisters();
       space->RestoreStateOnSwitch();

       machine->Run();
    }
    else if ((which == SyscallException) && (type == SYScall_Fork)){
       IntStatus oldLevel = interrupt->SetLevel(IntOff);
       char* buff = new char[8];
       buff[0]= machine->pidIndex+'0';
       buff[1]=' ';
       buff[2]='C';
       buff[3]='H';
       buff[4]='I';
       buff[5]='L';
       buff[6]='D';
       buff[7]='\0';
       NachOSThread* childThread = new NachOSThread(buff);
// Creating the addr space for the child process
       int NumOfPages = currentThread->space->GetNumPagesInVM();
       childThread->space =new ProcessAddrSpace(NumOfPages);
       childThread->space->SetNumPagesInVM(NumOfPages);
//creating a temporary nachostable 
       TranslationEntry* tmpNachOSTable = new TranslationEntry[NumOfPages];
       //copying the NachOSpageTable of parent to tmp
       currentThread->space->CopyAddrSpace(NumOfPages, tmpNachOSTable);
       int parstartaddr = tmpNachOSTable[0].physicalPage*PageSize;
       //assigning new physical page for child
       for(int i=0;i<NumOfPages;i++){
         tmpNachOSTable[i].physicalPage += machine->WrittenPages;
       }
       int childstartaddr = tmpNachOSTable[0].physicalPage*PageSize;
       //assigning the tmpNachOSTable to child 
       childThread->space->AssignAddrSpace(NumOfPages, tmpNachOSTable);
       
// Copying the main memory
       for(int i=0;i<NumOfPages*PageSize;i++){
         machine->mainMemory[i+childstartaddr] = machine->mainMemory[i+parstartaddr];
       }
       
       currentThread->space->RestoreStateOnSwitch();
      // Set the return value 0 for child and update PC reg
       machine->WriteRegister(2,0);
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
       // Save the current user regs in child
       childThread->SaveUserState();
      // Allocate the stack for the child
       childThread->ThreadFork(startFunc,0); 
       // settin the return value for parent
       machine->WriteRegister(2,childThread->getPID());
       (void)interrupt->SetLevel(oldLevel);
    }
    else if ((which == SyscallException) && (type == SYScall_Join)){

       int childpid = machine->ReadRegister(4);
       //get current pid
       int pid = currentThread->getPID();
       //set the interrupts to off
       IntStatus oldLevel = interrupt->SetLevel(IntOff);
      //if parent of child thread is not same as current thread
       if(machine->pidData[1][childpid] != pid){
         machine->WriteRegister(2,-1);
       }
       //if child is already dead then return exit code and reset
       else if(machine->pidData[0][childpid] == -2){
         machine->WriteRegister(2,machine->pidData[2][childpid]);

         machine->pidData[0][childpid] = 0;
         machine->pidData[1][childpid] = 0;
         machine->pidData[2][childpid] = 0;

         currentThread->pidPointer[childpid] = NULL;
       }
      //if not dead put current thread to sleep waiting for the given child thread and reset on waking up
       else{
         currentThread->waitForPID = childpid;
         currentThread->PutThreadToSleep();
         
         currentThread->waitForPID = 0;
         machine->WriteRegister(2,machine->pidData[2][childpid]);

         machine->pidData[0][childpid] = 0;
         machine->pidData[1][childpid] = 0;
         machine->pidData[2][childpid] = 0;

         currentThread->pidPointer[childpid] = NULL;
       }
         
       (void)interrupt->SetLevel(oldLevel);
       
       machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
       machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
       machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    }
    else {
	printf("Unexpected user mode exception %d %d\n", which, type);
	ASSERT(FALSE);
    }
}

void startFunc(int arg){
    DEBUG('t', "Now in thread \"%s\"\n", currentThread->getName());

    // If the old thread gave up the processor because it was finishing,
    // we need to delete its carcass.  Note we cannot delete the thread
    // before now (for example, in NachOSThread::FinishThread()), because up to this
    // point, we were still running on the old thread's stack!
    if (threadToBeDestroyed != NULL) {
        delete threadToBeDestroyed;
	threadToBeDestroyed = NULL;
    }
    
#ifdef USER_PROGRAM
    if (currentThread->space != NULL) {		// if there is an address space
        currentThread->RestoreUserState();     // to restore, do it.
	currentThread->space->RestoreStateOnSwitch();
    }
#endif

    machine->Run();
}
