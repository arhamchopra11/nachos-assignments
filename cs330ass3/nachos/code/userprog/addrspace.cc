// addrspace.cc 
//	Routines to manage address spaces (executing user programs).
//
//	In order to run a user program, you must:
//
//	1. link with the -N -T 0 option 
//	2. run coff2noff to convert the object file to Nachos format
//		(Nachos object code format is essentially just a simpler
//		version of the UNIX executable object code format)
//	3. load the NOFF file into the Nachos file system
//		(if you haven't implemented the file system yet, you
//		don't need to do this last step)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "addrspace.h"
#include "iostream"
using namespace std;
//----------------------------------------------------------------------
// SwapHeader
// 	Do little endian to big endian conversion on the bytes in the 
//	object file header, in case the file was generated on a little
//	endian machine, and we're now running on a big endian machine.
//----------------------------------------------------------------------

static void 
SwapHeader (NoffHeader *noffH)
{
	noffH->noffMagic = WordToHost(noffH->noffMagic);
	noffH->code.size = WordToHost(noffH->code.size);
	noffH->code.virtualAddr = WordToHost(noffH->code.virtualAddr);
	noffH->code.inFileAddr = WordToHost(noffH->code.inFileAddr);
	noffH->initData.size = WordToHost(noffH->initData.size);
	noffH->initData.virtualAddr = WordToHost(noffH->initData.virtualAddr);
	noffH->initData.inFileAddr = WordToHost(noffH->initData.inFileAddr);
	noffH->uninitData.size = WordToHost(noffH->uninitData.size);
	noffH->uninitData.virtualAddr = WordToHost(noffH->uninitData.virtualAddr);
	noffH->uninitData.inFileAddr = WordToHost(noffH->uninitData.inFileAddr);
}

//----------------------------------------------------------------------
// ProcessAddrSpace::ProcessAddrSpace
// 	Create an address space to run a user program.
//	Load the program from a file "executable", and set everything
//	up so that we can start executing user instructions.
//
//	Assumes that the object code file is in NOFF format.
//
//	First, set up the translation from program memory to physical 
//	memory.  For now, this is really simple (1:1), since we are
//	only uniprogramming, and we have a single unsegmented page table
//
//	"executable" is the file containing the object code to load into memory
//----------------------------------------------------------------------

ProcessAddrSpace::ProcessAddrSpace(OpenFile *executable)
{
    NoffHeader noffH;
    unsigned int i, size;
    unsigned vpn, offset;
    TranslationEntry *entry;
    unsigned int pageFrame;
    numSharedPages=0;     
// Getting the no of pages in exec
    executable->ReadAt((char *)&noffH, sizeof(noffH), 0);
    if ((noffH.noffMagic != NOFFMAGIC) && 
		(WordToHost(noffH.noffMagic) == NOFFMAGIC))
    	SwapHeader(&noffH);
    ASSERT(noffH.noffMagic == NOFFMAGIC);
    size = noffH.code.size + noffH.initData.size + noffH.uninitData.size 
			+ UserStackSize;	// we need to increase the size
						// to leave room for the stack
    numPagesInVM = divRoundUp(size, PageSize);
    size = numPagesInVM * PageSize;
    DEBUG('a', "Initializing address space, num pages %d, size %d\n", 
					numPagesInVM, size);
// first, set up the translation 
    NachOSpageTable = new TranslationEntry[numPagesInVM];
    for (i = 0; i < numPagesInVM; i++) {
	NachOSpageTable[i].virtualPage = i;
	NachOSpageTable[i].physicalPage = -1;
	NachOSpageTable[i].valid = FALSE;
	NachOSpageTable[i].use = FALSE;
	NachOSpageTable[i].dirty = FALSE;
   NachOSpageTable[i].shared = FALSE;
	NachOSpageTable[i].readOnly = FALSE;  // if the code segment was entirely on 
   NachOSpageTable[i].cur_pid = currentThread->GetPID();
   NachOSpageTable[i].Backup=FALSE;
					// a separate page, we could set its 
					// pages to be read-only
    }
    
    backup_mem = new char[numPagesInVM*PageSize];

  
}

ProcessAddrSpace::ProcessAddrSpace(int numPages){
   int size = numPages * PageSize;
   
   numPagesInVM = numPages;
    DEBUG('a', "Initializing address space, num pages %d, size %d\n", 
					numPagesInVM, size);
// first, set up the translation 
    NachOSpageTable = new TranslationEntry[numPagesInVM];
    for (int i = 0; i < numPagesInVM; i++) {
	NachOSpageTable[i].virtualPage = i;
	NachOSpageTable[i].physicalPage = -1;
	NachOSpageTable[i].valid = FALSE;
	NachOSpageTable[i].use = FALSE;
	NachOSpageTable[i].dirty = FALSE;
   // There can be no shared memory during the start of the program
   NachOSpageTable[i].shared = FALSE;
	NachOSpageTable[i].readOnly = FALSE;  // if the code segment was entirely on 
   NachOSpageTable[i].cur_pid = currentThread->c_pid;
   NachOSpageTable[i].Backup=FALSE;
					// a separate page, we could set its 
					// pages to be read-only
    }
    cout<<"Created a new tranlation page"<<endl;
    
    backup_mem = new char[numPagesInVM*PageSize];
    cout<<"MADE AN EMPTY CONSTRUCT"<<endl;
}



void ProcessAddrSpace::getPhyPage(int vaddr,OpenFile *executable){
   
    
   TranslationEntry *entry;
   int vpn = vaddr/PageSize;
   int offset = vaddr%PageSize;
   NoffHeader noffH;

   stats->numPageFaults++;

   executable->ReadAt((char *)&noffH, sizeof(noffH), 0);
    if ((noffH.noffMagic != NOFFMAGIC) && 
		(WordToHost(noffH.noffMagic) == NOFFMAGIC))
    	SwapHeader(&noffH);
    ASSERT(noffH.noffMagic == NOFFMAGIC);
  
   int i;
   int pageToAllocate=-1;
   for(i=0;i<NumPhysPages;i++){
      if(InvertTable[i].valid==FALSE){
         pageToAllocate=i;
         break;
      }    
   }
   
   if(pageToAllocate==-1){
      int p=-1;
      switch(replacementAlgo){
         case 1: cout<<"Calling "<<replacementAlgo<<" Replacement"<<endl;pageToAllocate=getRepPageFIFO(p); break;
         case 2: cout<<"Calling "<<replacementAlgo<<" Replacement"<<endl;pageToAllocate=getRepPageRandom(p); break;
         case 3: cout<<"Calling "<<replacementAlgo<<" Replacement"<<endl;pageToAllocate=getRepPageLRU(p); break;
         case 4: cout<<"Calling "<<replacementAlgo<<" Replacement"<<endl;pageToAllocate=getRepPageLRU_CLOCK(p); break;
         default: cout<<"Found no replacementAlgo"<<endl;ASSERT(FALSE);
      }
      numPagesAllocated--;
      if(InvertTable[pageToAllocate].dirty==TRUE) {
         SavePage(pageToAllocate);
      }
      int other_vpn = InvertTable[pageToAllocate].virtualPage;
      InvertTable[pageToAllocate].valid = FALSE;
      
      int pid = InvertTable[pageToAllocate].cur_pid;
      NachOSThread* th = threadArray[pid];
      th->space->invalidatePage(other_vpn);
      
   }
   else{
      LRU_CLOCK_Array[pageToAllocate][0]=pageToAllocate;
      LRU_CLOCK_Array[pageToAllocate][1]=1;
   }
   

   //Should be run iff the pageToAllocate is an unvalid page.
   ASSERT(pageToAllocate!=-1);
   ASSERT(InvertTable[pageToAllocate].valid==FALSE);
   
   
	NachOSpageTable[vpn].physicalPage = pageToAllocate;
	NachOSpageTable[vpn].valid = TRUE;
	NachOSpageTable[vpn].use = FALSE;
	NachOSpageTable[vpn].dirty = FALSE;
//********************************************************************
   NachOSpageTable[vpn].shared = FALSE;
//********************************************************************
	NachOSpageTable[vpn].readOnly = FALSE;

   InvertTable[pageToAllocate].virtualPage=vpn;
   InvertTable[pageToAllocate].cur_pid = currentThread->GetPID();
	InvertTable[pageToAllocate].valid = TRUE;
	InvertTable[pageToAllocate].use = FALSE;
	InvertTable[pageToAllocate].dirty = FALSE;
//********************************************************************
   InvertTable[pageToAllocate].shared = FALSE;
//********************************************************************
	InvertTable[pageToAllocate].readOnly = FALSE;

   switch(replacementAlgo){
      case 1: InvertTable[pageToAllocate].startCounter = pageStartCounter++;break;
      case 2: break;
//      case 3: for(int i=0;i<NumPhysPages;i++){
//                if(InvertTable[i].valid==TRUE) InvertTable[i].LRUCounter++;
//                cout<<InvertTable[i].LRUCounter<<endl;
//              }
//              InvertTable[pageToAllocate].LRUCounter=0;
//              break;
      case 4: break;
   }

   numPagesAllocated++;
   bzero(&machine->mainMemory[pageToAllocate*PageSize], PageSize);


   if(NachOSpageTable[vpn].Backup==TRUE){
      for(int i=0;i<PageSize;i++){
         machine->mainMemory[pageToAllocate*PageSize+i]=backup_mem[vpn*PageSize+i];
      }
   }
   else{

      int vpagestartaddr = vpn*PageSize;
      int vgap = vpagestartaddr - noffH.code.virtualAddr;
      
          int infileaddr = noffH.code.inFileAddr+vgap;


          entry = &NachOSpageTable[vpn];
          int pageFrame = entry->physicalPage;
          executable->ReadAt(&(machine->mainMemory[pageFrame * PageSize]),
           PageSize, infileaddr);
   }
  RestoreStateOnSwitch(); 
}

void ProcessAddrSpace::SavePage(int phypage){
   ASSERT(InvertTable[phypage].valid==TRUE)
   int pid = InvertTable[phypage].cur_pid;
   NachOSThread* th = threadArray[pid];
   int start_b = InvertTable[phypage].virtualPage*PageSize;
   
   int start_m = phypage*PageSize;
   
   for(int i=0;i<PageSize;i++) th->space->backup_mem[start_b+i]=machine->mainMemory[start_m+i];

   th->space->setBackupBit(start_b/PageSize);
}


int ProcessAddrSpace::getRepPageFIFO(int p){
   int page=-1;
   if(p==-1){
      int min=1000000;
      for(int i=0;i<NumPhysPages;i++){
         if(min>InvertTable[i].startCounter){
            min=InvertTable[i].startCounter;
            page=i;
         }
      }
   }
   else{
      int min=1000000;
      for(int i=0;i<NumPhysPages;i++){
         if(min>InvertTable[i].startCounter && i!=p){
            min=InvertTable[i].startCounter;
            page=i;
         }
      }
   }
   return page;
}

int ProcessAddrSpace::getRepPageRandom(int p){
   int page=-1;
   if(p==-1){
      page = Random()%NumPhysPages;   
   }
   else{
      page=p;
      while(page==p) page=Random()%NumPhysPages;
   }
   return page;
}

int ProcessAddrSpace::getRepPageLRU(int p){
   int page=-1;
   if(p==-1){
      int max=-1;

      for(int i=0;i<NumPhysPages;i++){
         if(max<InvertTable[i].LRUCounter){
            max=InvertTable[i].LRUCounter;
            page=i;
         }
      }
   }
   else{
      int max=-1;

      for(int i=0;i<NumPhysPages;i++){
         if(max<InvertTable[i].LRUCounter && i!=p){
            max=InvertTable[i].LRUCounter;
            page=i;
         }
      }
   }
   return page;
}

int ProcessAddrSpace::getRepPageLRU_CLOCK(int p){
   int page=-1;
   if(p==-1){
      for(int i=0;i<=NumPhysPages;i++){
         if(LRU_CLOCK_Array[(i+current_count+1)%NumPhysPages][1]==1){
            cout<<(i+current_count+1)%NumPhysPages<<endl;
            LRU_CLOCK_Array[(i+current_count+1)%NumPhysPages][1]=0;
         }
         else{
            current_count=(i+current_count+1)%NumPhysPages;
            page=current_count;
            LRU_CLOCK_Array[page][1]=1;
            break;
         }
      }
   }
   else{// Not Implemented Correctly
   }
   ASSERT(page!=-1);
   return page;
}

//----------------------------------------------------------------------
// ProcessAddrSpace::ProcessAddrSpace (ProcessAddrSpace*) is called by a forked thread.
//      We need to duplicate the address space of the parent.
//----------------------------------------------------------------------
int ProcessAddrSpace::AttachSHM(int shmemory){
    int numOldPages = numPagesInVM;
    int numShmPages = divRoundUp(shmemory, PageSize);
    numSharedPages +=numShmPages;
    unsigned i, size = numOldPages * PageSize + numShmPages * PageSize;
    numPagesInVM = numOldPages + numShmPages;
    DEBUG('a', "Initializing address space, num pages %d, size %d\n",
                                        numOldPages+numShmPages, size);

    TranslationEntry* NewNachOSpageTable = new TranslationEntry[numPagesInVM];
    for (i = 0; i < numOldPages; i++) {
        NewNachOSpageTable[i].virtualPage = i;
        NewNachOSpageTable[i].physicalPage = NachOSpageTable[i].physicalPage;
        NewNachOSpageTable[i].shared = NachOSpageTable[i].shared;
        NewNachOSpageTable[i].valid = NachOSpageTable[i].valid;
        NewNachOSpageTable[i].use = NachOSpageTable[i].use;
        NewNachOSpageTable[i].dirty = NachOSpageTable[i].dirty;
        NewNachOSpageTable[i].readOnly = NachOSpageTable[i].readOnly;  	
    }
    
    for (i = numOldPages; i < numPagesInVM; i++) {
        NewNachOSpageTable[i].virtualPage = i;
        NewNachOSpageTable[i].physicalPage = i - numOldPages + numPagesAllocated;
        NewNachOSpageTable[i].shared = TRUE; 
        NewNachOSpageTable[i].valid = TRUE;
        NewNachOSpageTable[i].use = FALSE;
        NewNachOSpageTable[i].dirty = FALSE;
        NewNachOSpageTable[i].readOnly = FALSE;
    }
    unsigned startAddrChild = numOldPages*PageSize;
//    for (i=0; i<numShmPages*PageSize; i++) {
//       machine->mainMemory[startAddrChild+i] = 0;
//    }
//    delete oldPageTable;
    numPagesAllocated += numShmPages;
    TranslationEntry* oldPageTable = NachOSpageTable;
    NachOSpageTable = NewNachOSpageTable;
    delete oldPageTable;
    RestoreStateOnSwitch();

    return startAddrChild;

}
void ProcessAddrSpace::Reset(){
   for(int i=0;i<numPagesInVM;i++){
      if(NachOSpageTable[i].valid==TRUE) InvertTable[NachOSpageTable[i].physicalPage].valid=FALSE;
   }
}

void ProcessAddrSpace::CopySpace(ProcessAddrSpace *parentSpace)
{
    numPagesInVM = parentSpace->GetNumPages();
    unsigned i, size = (numPagesInVM) * PageSize;

//    ASSERT(numPagesInVM+numPagesAllocated <= NumPhysPages);                // check we're not trying
                                                                                // to run anything too big --
                                                                                // at least until we have
                                                                                // virtual memory
    DEBUG('a', "Initializing address space, num pages %d, size %d\n",
                                        numPagesInVM, size);
    // first, set up the translation
    TranslationEntry* parentPageTable = parentSpace->GetPageTable();
    int sharedpages = 0;
    int invalidpages = 0;
    
    // Copying the back memory
    for(int i=0;i<numPagesInVM*PageSize;i++){
      backup_mem[i] = parentSpace->backup_mem[i];
    }
    
    
    
    for (i = 0; i < numPagesInVM; i++) {
        if(parentPageTable[i].shared==FALSE){
           if(parentPageTable[i].valid==TRUE){
              
              int pageToCopy=i;
              stats->numPageFaults++;
              int pageToAllocate=-1;
              for(int i=0;i<NumPhysPages;i++){
                 if(InvertTable[i].valid==FALSE){
                    pageToAllocate=i;
                    break;
                 }
              }

               if(pageToAllocate==-1){
                  switch(replacementAlgo){
                     case 1: cout<<"Calling "<<replacementAlgo<<" Replacement"<<endl;pageToAllocate=getRepPageFIFO(parentPageTable[pageToCopy].physicalPage); break;
                     case 2: cout<<"Calling "<<replacementAlgo<<" Replacement"<<endl;pageToAllocate=getRepPageRandom(parentPageTable[pageToCopy].physicalPage); break;
                     case 3: cout<<"Calling "<<replacementAlgo<<" Replacement"<<endl;pageToAllocate=getRepPageLRU(parentPageTable[pageToCopy].physicalPage); break;
                     case 4: cout<<"Calling "<<replacementAlgo<<" Replacement"<<endl;pageToAllocate=getRepPageLRU_CLOCK(parentPageTable[pageToCopy].physicalPage); break;
                     default: cout<<"Found no replacementAlgo"<<endl;ASSERT(FALSE);
                  }
                  numPagesAllocated--;
                  if(InvertTable[pageToAllocate].dirty==TRUE) {
                     SavePage(pageToAllocate);
                  }
                  int other_vpn = InvertTable[pageToAllocate].virtualPage;
                  InvertTable[pageToAllocate].valid = FALSE;
                  
                  int pid = InvertTable[pageToAllocate].cur_pid;
                  NachOSThread* th = threadArray[pid];
                  th->space->invalidatePage(other_vpn);
               }
               else{
                  LRU_CLOCK_Array[pageToAllocate][0]=pageToAllocate;
                  LRU_CLOCK_Array[pageToAllocate][1]=1;
               }
               
               //Should be run iff the pageToAllocate is an unvalid page.
               
               ASSERT(pageToAllocate!=-1);
               ASSERT(InvertTable[pageToAllocate].valid==FALSE);
               NachOSpageTable[pageToCopy].virtualPage = pageToCopy; 
               NachOSpageTable[pageToCopy].physicalPage=pageToAllocate;
               NachOSpageTable[pageToCopy].shared = FALSE;
               NachOSpageTable[pageToCopy].valid = TRUE;
               NachOSpageTable[pageToCopy].use = parentPageTable[pageToCopy].use;
               NachOSpageTable[pageToCopy].dirty = parentPageTable[pageToCopy].dirty;
               NachOSpageTable[pageToCopy].readOnly = parentPageTable[pageToCopy].readOnly;  	// if the code segment was entirely on
               NachOSpageTable[pageToCopy].Backup = parentPageTable[pageToCopy].Backup;

               InvertTable[pageToAllocate].virtualPage = pageToCopy;
               InvertTable[pageToAllocate].physicalPage = pageToAllocate;
               InvertTable[pageToAllocate].dirty = parentPageTable[pageToCopy].dirty;
               InvertTable[pageToAllocate].valid = TRUE;
               InvertTable[pageToAllocate].use = parentPageTable[pageToCopy].use;
               InvertTable[pageToAllocate].cur_pid = currentThread->c_pid;
               InvertTable[pageToAllocate].readOnly = parentPageTable[pageToCopy].readOnly;  	// if the code segment was entirely on
               InvertTable[pageToAllocate].shared = FALSE;
               
               numPagesAllocated++;
               switch(replacementAlgo){
                  case 1: InvertTable[pageToAllocate].startCounter=pageStartCounter++;break;
               }

 
///              bzero(&machine->mainMemory[parentPageTable[pageToCopy].physicalPage*PageSize],PageSize);
              for(int j=0;j<PageSize;j++){
                 machine->mainMemory[PageSize*pageToAllocate+j] = machine->mainMemory[PageSize*parentPageTable[pageToCopy].physicalPage+j];
              }
           }
           else{
               NachOSpageTable[i].virtualPage = i;
               NachOSpageTable[i].physicalPage = -1;
               NachOSpageTable[i].shared = FALSE;
               NachOSpageTable[i].valid = FALSE;
               NachOSpageTable[i].use = FALSE;
               NachOSpageTable[i].dirty = FALSE;
               NachOSpageTable[i].readOnly = parentPageTable[i].readOnly;  	// if the code segment was entirely on
               NachOSpageTable[i].Backup = parentPageTable[i].Backup;
               invalidpages ++;
           }
        }
        else{
            NachOSpageTable[i].virtualPage = i;
            NachOSpageTable[i].physicalPage = parentPageTable[i].physicalPage;
            NachOSpageTable[i].shared = TRUE;
            NachOSpageTable[i].valid = TRUE;
            NachOSpageTable[i].use = parentPageTable[i].use;
            NachOSpageTable[i].dirty = parentPageTable[i].dirty;
            NachOSpageTable[i].readOnly = parentPageTable[i].readOnly;  	// if the code segment was entirely on
            NachOSpageTable[i].Backup = parentPageTable[i].Backup;
           stats->numPageFaults++;
           sharedpages ++;
        }
                                                   // a separate page, we could set its
                                                   // pages to be read-only
    }
   
}

//----------------------------------------------------------------------
// ProcessAddrSpace::~ProcessAddrSpace
// 	Dealloate an address space.  Nothing for now!
//----------------------------------------------------------------------

ProcessAddrSpace::~ProcessAddrSpace()
{
   delete NachOSpageTable;
}

//----------------------------------------------------------------------
// ProcessAddrSpace::InitUserCPURegisters
// 	Set the initial values for the user-level register set.
//
// 	We write these directly into the "machine" registers, so
//	that we can immediately jump to user code.  Note that these
//	will be saved/restored into the currentThread->userRegisters
//	when this thread is context switched out.
//----------------------------------------------------------------------

void
ProcessAddrSpace::InitUserCPURegisters()
{
    int i;

    for (i = 0; i < NumTotalRegs; i++)
	machine->WriteRegister(i, 0);

    // Initial program counter -- must be location of "Start"
    machine->WriteRegister(PCReg, 0);	

    // Need to also tell MIPS where next instruction is, because
    // of branch delay possibility
    machine->WriteRegister(NextPCReg, 4);

   // Set the stack register to the end of the address space, where we
   // allocated the stack; but subtract off a bit, to make sure we don't
   // accidentally reference off the end!
    machine->WriteRegister(StackReg, numPagesInVM * PageSize - 16);
    DEBUG('a', "Initializing stack register to %d\n", numPagesInVM * PageSize - 16);
}

//----------------------------------------------------------------------
// ProcessAddrSpace::SaveStateOnSwitch
// 	On a context switch, save any machine state, specific
//	to this address space, that needs saving.
//
//	For now, nothing!
//----------------------------------------------------------------------

void ProcessAddrSpace::SaveStateOnSwitch() 
{}

//----------------------------------------------------------------------
// ProcessAddrSpace::RestoreStateOnSwitch
// 	On a context switch, restore the machine state so that
//	this address space can run.
//
//      For now, tell the machine where to find the page table.
//----------------------------------------------------------------------

void ProcessAddrSpace::RestoreStateOnSwitch() 
{
    machine->NachOSpageTable = NachOSpageTable;
    machine->NachOSpageTableSize = numPagesInVM;
}

unsigned
ProcessAddrSpace::GetNumPages()
{
   return numPagesInVM;
}

TranslationEntry*
ProcessAddrSpace::GetPageTable()
{
   return NachOSpageTable;
}
