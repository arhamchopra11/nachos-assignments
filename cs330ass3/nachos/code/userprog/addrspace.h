// addrspace.h 
//	Data structures to keep track of executing user programs 
//	(address spaces).
//
//	For now, we don't keep any information about address spaces.
//	The user level CPU state is saved and restored in the thread
//	executing the user program (see thread.h).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef ADDRSPACE_H
#define ADDRSPACE_H

#include "copyright.h"
#include "filesys.h"
#include "noff.h"

#define UserStackSize		1024 	// increase this as necessary!

class ProcessAddrSpace {
  public:
    ProcessAddrSpace(OpenFile *executable);	// Create an address space,
					// initializing it with the program
					// stored in the file "executable"

    ProcessAddrSpace (ProcessAddrSpace *parentSpace);	// Used by fork
    
    ProcessAddrSpace (ProcessAddrSpace *parentSpace, int shmememory);	// Used by Shm Allocate to allocate Shared Memory
    ProcessAddrSpace (int numPages);
    ~ProcessAddrSpace();			// De-allocate an address space

    void InitUserCPURegisters();		// Initialize user-level CPU registers,
					// before jumping to user code

    void SaveStateOnSwitch();			// Save/restore address space-specific
    void RestoreStateOnSwitch();		// info on a context switch
//***************************************************************************
    void getPhyPage(int addr, OpenFile* executable);
    void CopySpace(ProcessAddrSpace* parentSpace);
//***************************************************************************

    unsigned GetNumPages();
    int AttachSHM(int shmemory);
    TranslationEntry* GetPageTable();
   
    int getRepPageFIFO(int p);
    int getRepPageRandom(int p);
    int getRepPageLRU(int p);
    int getRepPageLRU_CLOCK(int p);
    void setBackupBit(int vpn) { NachOSpageTable[vpn].Backup=TRUE; }
    void invalidatePage(int vpn){ NachOSpageTable[vpn].valid=FALSE; }
    void SavePage(int phypage);
    void Reset();
  private:
    TranslationEntry *NachOSpageTable;	// Assume linear page table translation
					// for now!
    unsigned int numPagesInVM;		// Number of pages in the virtual 
					// address space
  public:
    int numSharedPages;
    char* backup_mem;
};

#endif // ADDRSPACE_H
