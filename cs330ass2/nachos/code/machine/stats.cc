// stats.h 
//	Routines for managing statistics about Nachos performance.
//
// DO NOT CHANGE -- these stats are maintained by the machine emulation.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "utility.h"
#include "stats.h"
#include "math.h"
//----------------------------------------------------------------------
// Statistics::Statistics
// 	Initialize performance metrics to zero, at system startup.
//----------------------------------------------------------------------

Statistics::Statistics()
{
    for(int i=0;i<MAX_NO_THREAD;i++){
       waitArray[i]=0;
       complArray[i]=0;
    }
    CPUBurstCounter=0;
    totCPUBurstTime=0;
    maxCPUBurstTime=0;
    minCPUBurstTime=100000000;

    totErrorEst=0;

    totWaitTime=0;
    waitCounter=0;

    totalTicks = idleTicks = systemTicks = userTicks = 0;
    numDiskReads = numDiskWrites = 0;
    numConsoleCharsRead = numConsoleCharsWritten = 0;
    numPageFaults = numPacketsSent = numPacketsRecvd = 0;
}

//----------------------------------------------------------------------
// Statistics::Print
// 	Print performance metrics, when we've finished everything
//	at system shutdown.
//----------------------------------------------------------------------

void
Statistics::Print()
{
    long long int avgWaitTime=0,maxWaitTime=0,minWaitTime=100000000,varWaitTime=0;
    long long int avgComplTime=0,maxComplTime=0,minComplTime=100000000,varComplTime=0;
    for(int i=1;i<waitCounter;i++){
       if(waitArray[i]>maxWaitTime) maxWaitTime=waitArray[i];
       if(waitArray[i]<minWaitTime) minWaitTime=waitArray[i];

       avgWaitTime+=waitArray[i];
       
       if(complArray[i]>maxComplTime) maxComplTime=complArray[i];
       if(complArray[i]<minComplTime) minComplTime=complArray[i];

       avgComplTime+=complArray[i];
    }
    if(waitCounter==1) {
       maxWaitTime=waitArray[0];
       minWaitTime=waitArray[0];
       avgWaitTime=waitArray[0];
       
       maxComplTime=complArray[0];
       minComplTime=complArray[0];
       avgComplTime=complArray[0];
    }
    else{
       avgWaitTime/=(waitCounter-1);
       avgComplTime/=(waitCounter-1);
    }

    for(int i=1;i<waitCounter;i++){
       varWaitTime+=(avgWaitTime-waitArray[i])*(avgWaitTime-waitArray[i]);
       
       varComplTime+=(avgComplTime-complArray[i])*(avgComplTime-complArray[i]);
    }

    if(waitCounter==1) {
       varWaitTime=0;
       varComplTime=0;
    }
    else {
       varWaitTime/=(waitCounter-1);
       
       varComplTime/=(waitCounter-1);
    }

    printf("Total CPU Busy Time: %lld\n",totCPUBurstTime);
    printf("Total Execution Time: %lld\n",totalTicks);
    printf("CPU Utilization: %f\n",(float)totCPUBurstTime*100/totalTicks);
    printf("Maximum CPU burst length: %lld\n",maxCPUBurstTime);
    printf("Minimum CPU burst length: %lld\n",minCPUBurstTime);
    printf("Average CPU burst length: %lld\n",totCPUBurstTime/CPUBurstCounter);
    printf("Number of non-zero CPU bursts observed : %lld\n", CPUBurstCounter);
    printf("Maximum Wait Time : %lld\n",maxWaitTime);
    printf("Minimum Wait Time : %lld\n",minWaitTime);
    printf("Average Wait Time : %lld\n",avgWaitTime);
    printf("Variance Wait Time : %lld\n",varWaitTime);
    
    printf("Maximum Completion Time : %lld\n",maxComplTime);
    printf("Minimum Completion Time : %lld\n",minComplTime);
    printf("Average Completion Time : %lld\n",avgComplTime);
    printf("Variance Completion Time : %lld\n",varComplTime);

    printf("Estimatation Error : %lld\n",totErrorEst);

    printf("Ticks: total %d, idle %d, system %d, user %d\n", totalTicks, 
	idleTicks, systemTicks, userTicks);
    printf("Disk I/O: reads %d, writes %d\n", numDiskReads, numDiskWrites);
    printf("Console I/O: reads %d, writes %d\n", numConsoleCharsRead, 
	numConsoleCharsWritten);
    printf("Paging: faults %d\n", numPageFaults);
    printf("Network I/O: packets received %d, sent %d\n", numPacketsRecvd, 
	numPacketsSent);
}
