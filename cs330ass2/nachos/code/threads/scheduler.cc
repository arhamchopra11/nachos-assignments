// scheduler.cc 
//	Routines to choose the next thread to run, and to dispatch to
//	that thread.
//
// 	These routines assume that interrupts are already disabled.
//	If interrupts are disabled, we can assume mutual exclusion
//	(since we are on a uniprocessor).
//
// 	NOTE: We can't use Locks to provide mutual exclusion here, since
// 	if we needed to wait for a lock, and the lock was busy, we would 
//	end up calling FindNextThreadToRun(), and that would put us in an 
//	infinite loop.
//
// 	Very simple implementation -- no priorities, straight FIFO.
//	Might need to be improved in later assignments.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "scheduler.h"
#include "system.h"
#include "iostream"
using namespace std;
//----------------------------------------------------------------------
// NachOSscheduler::NachOSscheduler
// 	Initialize the list of ready but not running threads to empty.
//----------------------------------------------------------------------

NachOSscheduler::NachOSscheduler()
{ 
    readyThreadList = new List; 
    tmpThreadList = new List; 
} 

//----------------------------------------------------------------------
// NachOSscheduler::~NachOSscheduler
// 	De-allocate the list of ready threads.
//----------------------------------------------------------------------

NachOSscheduler::~NachOSscheduler()
{ 
    delete readyThreadList; 
    delete tmpThreadList; 
} 

//----------------------------------------------------------------------
// NachOSscheduler::ThreadIsReadyToRun
// 	Mark a thread as ready, but not running.
//	Put it on the ready list, for later scheduling onto the CPU.
//
//	"thread" is the thread to be put on the ready list.
//----------------------------------------------------------------------

void
NachOSscheduler::ThreadIsReadyToRun (NachOSThread *thread)
{
    DEBUG('t', "Putting thread %s with PID %d on ready list.\n", thread->getName(), thread->GetPID());

    thread->setStatus(READY);
    if(machine->schedAlgo == 2){
//       cout<<"IN SCHED ALGO 2"<<endl;
       readyThreadList->SortedInsert((void*)thread,thread->predBurstTime);
    }
    else{
//       cout<<"IN SCHED ALGO 1/3/4"<<endl;
       readyThreadList->Append((void *)thread);
    }
}

//----------------------------------------------------------------------
// NachOSscheduler::FindNextThreadToRun
// 	Return the next thread to be scheduled onto the CPU.
//	If there are no ready threads, return NULL.
// Side effect:
//	Thread is removed from the ready list.
//----------------------------------------------------------------------

NachOSThread *
NachOSscheduler::FindNextThreadToRun ()
{
    if(machine->schedAlgo==1 || machine->schedAlgo==2){
       return (NachOSThread *)readyThreadList->Remove();
    }
    else if(machine->schedAlgo==3){
      NachOSThread* new_thread =(NachOSThread*) readyThreadList->Remove();
      if(currentThread->getStatus()==BLOCKED) return new_thread;
      else{
         if(new_thread==NULL) return currentThread;
         else return new_thread;
      }
    }
    else{
      if(currentThread->endTime-currentThread->startTime!=0){
         for(int i=0;i<thread_index;i++){
            if(!exitThreadArray[i]){
               int CPUBurst = (threadArray[i])->endTime - (threadArray[i])->startTime;
               cout<<CPUBurst<<"CPU BURST"<<endl;
               CPUArray[i]=CPUArray[i]+CPUBurst;
               cout<<CPUArray[i]<<"Old CPU Array"<<endl;
               CPUArray[i]/=2;
               cout<<CPUArray[i]<<"Old CPU Array"<<endl;
               threadArray[i]->endTime=0;
               threadArray[i]->startTime=0;

               priorityArray[i] = CPUArray[i]/2+threadArray[i]->basePriority;
               cout<<priorityArray[i]<<endl;
            }
         }
      }

      NachOSThread* min_thread = (NachOSThread*) readyThreadList->Remove();
      if(min_thread != NULL){
         int min_pri = priorityArray[min_thread->GetPID()];
         NachOSThread* new_thread=(NachOSThread*) readyThreadList->Remove();
         while(new_thread!=NULL){
            int new_pri = priorityArray[new_thread->GetPID()];
            if(new_pri<min_pri){
               NachOSThread* tmp = min_thread;
               min_thread=new_thread;
               new_thread=tmp;
               min_pri=new_pri;
            }
            tmpThreadList->Append((void *)new_thread);
            new_thread = (NachOSThread*) readyThreadList->Remove();
         }
         
         
         new_thread=(NachOSThread*) tmpThreadList->Remove();
         while(new_thread!=NULL){
          //  cout<<"WHILE New_Thread is not null"<<endl;
            readyThreadList->Append((void *)new_thread);
            new_thread = (NachOSThread*) tmpThreadList->Remove();
         }
         
         int cur_pri = priorityArray[currentThread->GetPID()];
         if(currentThread->getStatus()==BLOCKED){
            return min_thread;
         }
         else{
            if(cur_pri<min_pri){
               readyThreadList->Append((void *)min_thread);
               return currentThread;
            }
            else{
               return min_thread;
            }
         }
      }
      else{
         if(currentThread->getStatus()==BLOCKED) return NULL;
         else return currentThread;
      }
    }
}

//----------------------------------------------------------------------
// NachOSscheduler::Schedule
// 	Dispatch the CPU to nextThread.  Save the state of the old thread,
//	and load the state of the new thread, by calling the machine
//	dependent context switch routine, SWITCH.
//
//      Note: we assume the state of the previously running thread has
//	already been changed from running to blocked or ready (depending).
// Side effect:
//	The global variable currentThread becomes nextThread.
//
//	"nextThread" is the thread to be put into the CPU.
//----------------------------------------------------------------------

void
NachOSscheduler::Schedule (NachOSThread *nextThread)
{
    NachOSThread *oldThread = currentThread;
    
#ifdef USER_PROGRAM			// ignore until running user programs 
    if (currentThread->space != NULL) {	// if this thread is a user program,
        currentThread->SaveUserState(); // save the user's CPU registers
	currentThread->space->SaveStateOnSwitch();
    }
#endif
    
    oldThread->CheckOverflow();		    // check if the old thread
					    // had an undetected stack overflow

    currentThread = nextThread;		    // switch to the next thread
    currentThread->setStatus(RUNNING);      // nextThread is now running
    
    DEBUG('t', "Switching from thread \"%s\" with pid %d to thread \"%s\" with pid %d\n",
	  oldThread->getName(), oldThread->GetPID(), nextThread->getName(), nextThread->GetPID());
    
    // This is a machine-dependent assembly language routine defined 
    // in switch.s.  You may have to think
    // a bit to figure out what happens after this, both from the point
    // of view of the thread and from the perspective of the "outside world".

    // Setting the end time for the process and printing the results
/*    oldThread->endTime = stats->totalTicks;
    
    oldThread->predBurstTime = oldThread->predBurstTime * 0.5 + (oldThread->endTime - oldThread->startTime)*0.5;
    
    cout<<"End Time: "<<oldThread->endTime<<" Start Time"<<oldThread->startTime<<endl; 
    cout<<"Time: "<<oldThread->endTime-oldThread->startTime<<endl; 
    
    stats->totTime += oldThread->endTime-oldThread->startTime; 
    stats->counter ++ ;
*/

    _SWITCH(oldThread, nextThread);
    
//    currentThread->startTime = stats->totalTicks;
   
    DEBUG('t', "Now in thread \"%s\" with pid %d\n", currentThread->getName(), currentThread->GetPID());

    // If the old thread gave up the processor because it was finishing,
    // we need to delete its carcass.  Note we cannot delete the thread
    // before now (for example, in NachOSThread::FinishThread()), because up to this
    // point, we were still running on the old thread's stack!
    if (threadToBeDestroyed != NULL) {
        delete threadToBeDestroyed;
	threadToBeDestroyed = NULL;
    }
    
#ifdef USER_PROGRAM
    if (currentThread->space != NULL) {		// if there is an address space
        currentThread->RestoreUserState();     // to restore, do it.
	currentThread->space->RestoreStateOnSwitch();
    }
#endif
}

//----------------------------------------------------------------------
// NachOSscheduler::Tail
//      This is the portion of NachOSscheduler::Schedule after _SWITCH(). This needs
//      to be executed in the startup function used in fork().
//----------------------------------------------------------------------

void
NachOSscheduler::Tail ()
{
    // If the old thread gave up the processor because it was finishing,
    // we need to delete its carcass.  Note we cannot delete the thread
    // before now (for example, in NachOSThread::FinishThread()), because up to this
    // point, we were still running on the old thread's stack!
    if (threadToBeDestroyed != NULL) {
        delete threadToBeDestroyed;
        threadToBeDestroyed = NULL;
    }

#ifdef USER_PROGRAM
    if (currentThread->space != NULL) {         // if there is an address space
        currentThread->RestoreUserState();     // to restore, do it.
        currentThread->space->RestoreStateOnSwitch();
    }
#endif
}

   
void
NachOSscheduler::NewProcess ()
{
// Setting the start time for the process    
//    currentThread->startTime = stats->totalTicks;
   
    // If the old thread gave up the processor because it was finishing,
    // we need to delete its carcass.  Note we cannot delete the thread
    // before now (for example, in NachOSThread::FinishThread()), because up to this
    // point, we were still running on the old thread's stack!
    if (threadToBeDestroyed != NULL) {
        delete threadToBeDestroyed;
        threadToBeDestroyed = NULL;
    }

#ifdef USER_PROGRAM
    currentThread->space->InitUserCPURegisters();     
    currentThread->space->RestoreStateOnSwitch();
#endif
}


//----------------------------------------------------------------------
// NachOSscheduler::Print
// 	Print the scheduler state -- in other words, the contents of
//	the ready list.  For debugging.
//----------------------------------------------------------------------
void
NachOSscheduler::Print()
{
    printf("Ready list contents:\n");
    readyThreadList->Mapcar((VoidFunctionPtr) ThreadPrint);
}
