// progtest.cc 
//	Test routines for demonstrating that Nachos can load
//	a user program and execute it.  
//
//	Also, routines for testing the Console hardware device.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "console.h"
#include "addrspace.h"
#include "synch.h"
#include "iostream"
using namespace std;
#define MaxBatchSize 200
//----------------------------------------------------------------------
// StartUserProcess
// 	Run a user program.  Open the executable, load it into
//	memory, and jump to it.
//----------------------------------------------------------------------

void
StartUserProcess(char *filename)
{
    OpenFile *executable = fileSystem->Open(filename);
    ProcessAddrSpace *space;

    if (executable == NULL) {
	printf("Unable to open file %s\n", filename);
	return;
    }
    space = new ProcessAddrSpace(executable);    
    currentThread->space = space;

    delete executable;			// close file

    space->InitUserCPURegisters();		// set the initial register values
    space->RestoreStateOnSwitch();		// load page table register

    machine->Run();			// jump to the user progam
    ASSERT(FALSE);			// machine->Run never returns;
					// the address space exits
					// by doing the syscall "exit"
}

void StartFunction(int dummy){
   currentThread->Start();
   machine->Run();
}

void StartBatchProcess(char* filename)
{
   FILE* fp = fopen(filename,"r");
   char* exec= new char[50];
   NachOSThread** thread_list = new NachOSThread*[MaxBatchSize];
// Getting the scheduling algorithm
   int ret = fscanf(fp,"%s",exec);
   int algo = atoi(exec);
   machine->schedAlgo = algo;
   int i=0;
// Getting the name of files and making threads with priorities
   ret=fscanf(fp,"%s",exec);
   while(ret!=EOF){
      cout<<exec<<endl;
       OpenFile* executable = fileSystem->Open(exec);
       ProcessAddrSpace *space;

       if (executable == NULL) {
         printf("Unable to open file %s\n", exec);
         return;
       }
       char* name = new char[7];
       name[0] = 'C';
       name[1] = 'h';
       name[2] = 'i';
       name[3] = 'l';
       name[4] = 'd';
       name[5]='0'+i;
       name[6]='\0';
       NachOSThread* new_child = new NachOSThread(name);
       thread_list[i++]=new_child;
       int pid_new = new_child->GetPID();
       space = new ProcessAddrSpace(executable);    
       new_child->space = space;
       new_child->AllocateThreadStack(StartFunction,0);
       delete executable;			// close file
       ret=fscanf(fp,"%s",exec);
       int pri_val = atoi(exec);
       if(pri_val==0 && exec[0]!='0'){
          cout<<"NO NUMBER"<<endl;
          priorityArray[pid_new]+=100;
          threadArray[pid_new]->basePriority+=100;
       }
       else{
          cout<<"NUMBER"<<endl;
          priorityArray[pid_new]+=pri_val;
          threadArray[pid_new]->basePriority+=pri_val;
          if(ret!=EOF) ret=fscanf(fp,"%s",exec);
       }
   }
   fclose(fp);
   IntStatus oldLevel = interrupt->SetLevel(IntOff);
   for(int j=0;j<i;j++){
      
      thread_list[j]->Schedule();
   }
   interrupt->SetLevel(oldLevel);
}



// Data structures needed for the console test.  Threads making
// I/O requests wait on a Semaphore to delay until the I/O completes.

static Console *console;
static Semaphore *readAvail;
static Semaphore *writeDone;

//----------------------------------------------------------------------
// ConsoleInterruptHandlers
// 	Wake up the thread that requested the I/O.
//----------------------------------------------------------------------

static void ReadAvail(int arg) { readAvail->V(); }
static void WriteDone(int arg) { writeDone->V(); }

//----------------------------------------------------------------------
// ConsoleTest
// 	Test the console by echoing characters typed at the input onto
//	the output.  Stop when the user types a 'q'.
//----------------------------------------------------------------------

void 
ConsoleTest (char *in, char *out)
{
    char ch;

    console = new Console(in, out, ReadAvail, WriteDone, 0);
    readAvail = new Semaphore("read avail", 0);
    writeDone = new Semaphore("write done", 0);
    
    for (;;) {
	readAvail->P();		// wait for character to arrive
	ch = console->GetChar();
	console->PutChar(ch);	// echo it!
	writeDone->P() ;        // wait for write to finish
	if (ch == 'q') return;  // if q, quit
    }
}
